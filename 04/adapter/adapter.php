<?php
/**
 *适配器模式
 *adapter
 */
class Target
{
	abstract public function request();
}
class Adaptee
{
	public function specificRequest()
	{
		echo '特殊请求<br />';
	}
}
class Adapter extends Target
{
	private $adaptee;
	
	public function __construct()
	{
		$this->adaptee = new Adaptee();
	}
	
	public function setAdaptee($adaptee)
	{
		$this->$adaptee = $adaptee;
	}
	
	public function getAdaptee()
	{
		return $this->adaptee;
	}
	
	
	public function request()
	{
		$this->adaptee->specificRequest();
	}
}
// test
Header('Content-Type:text/html;charset=utf-8');
$target = new Adapter();
$target->request();