<?php
abstract class AbstractRoad{
	protected $car;
	
	public function setCar($car){
		$this->car = $car;
	}
	public abstract function run();
}

abstract class AbstractCar{
	public abstract function run();
}

class SpeedWay extends AbstractRoad{
	public function run(){
		$this->car->run();
		printf('%s<br />','高速公路上行驶');
	}
}

class Street extends AbstractRoad{
	public function run(){
		$this->car->run();
		printf('%s<br />','市区街道上行驶');
	}
}

class Sidework extends AbstractRoad{
	public function run(){
		$this->car->run();
		printf('%s<br />','人行道上行驶');
	}
}

class Car extends AbstractCar{
	public function run(){
		printf('%s','小汽车在');
	}
}

class Bus extends AbstractCar{
	public function run(){
		printf('%s','巴士在');
	}
}

class Bike extends AbstractCar{
	public function run(){
		printf('%s','自行车在');
	}
}

//client
class Client{
	public static function main(){
		$road = new SpeedWay();
		$road->setCar(new Car());
		$road->run();
		$road->setCar(new Bus());
		$road->run();
		
		$road = new Street();
		$road->setCar(new Car());
		$road->run();
		$road->setCar(new Bus());
		$road->run();
		
		$road = new Sidework();
		$road->setCar(new Bike());
		$road->run();
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();