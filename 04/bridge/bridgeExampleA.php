<?php
//bridge
abstract class Implementor{
	public abstract function operation();
}

class ConcreteImplementorA extends Implementor{
	public function operation(){
		$format = '具体实现方法A的执行<br />';
		printf($format);
	}
}

class ConcreteImplementorB extends Implementor{
	public function operation(){
		$format = '具体实现方法B的执行<br />';
		printf($format);
	}
}

class Abstraction{
	protected $implementor;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function operation(){
		$this->implementor->operation();
	}
}

class RefinedAbstraction extends Abstraction{
	public function operation(){
		$this->implementor->operation();
	}
}

class Client{
	public static function main(){
		$ab = new RefinedAbstraction();
		
		$ab->__set('implementor',new ConcreteImplementorA());
		$ab->operation();
		
		$ab->__set('implementor',new ConcreteImplementorB());
		$ab->operation();
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();