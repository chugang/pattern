<?php
//手机软件
abstract class HandsetSoft{
	public abstract function run();
}

//手机游戏
class HandsetGame extends HandsetSoft{
	public function run(){
		printf('%s<br />','运行手机游戏');
	}
}

//手机通讯录
class HandsetAddressList extends HandsetSoft{
	public function run(){
		printf('%s<br />','运行手机通讯录');
	}
}

//MP3
class HandsetMusic extends HandsetSoft{
	public function run(){
		printf('%s<br />','运行手机MP3');
	}
}

//手机品牌
abstract class HandsetBrand{
	protected $soft;
	
	public function setHandsetSoft($soft){
		$this->soft = $soft;
	}
	
	//运行
	public abstract function run();
}

//手机品牌N
class HandSetBrandN extends HandsetBrand{
	public function run(){
		$this->soft->run();
	}
}

//手机品牌M
class HandSetBrandM extends HandsetBrand{
	public function run(){
		$this->soft->run();
	}
}

//手机品牌D
class HandSetBrandD extends HandsetBrand{
	public function run(){
		$this->soft->run();
	}
}

//client
class Client{
	public static function main(){
		$ab = new HandSetBrandN();
		$ab->setHandsetSoft(new HandsetGame());
		$ab->run();
		
		$ab->setHandsetSoft(new HandsetAddressList());
		$ab->run();
		
		$ab = new HandSetBrandM();
		$ab->setHandsetSoft(new HandsetGame());
		$ab->run();
		
		$ab->setHandsetSoft(new HandsetAddressList());
		$ab->run();
		
		$ab = new HandSetBrandD();
		$ab->setHandsetSoft(new HandsetMusic());
		$ab->run();
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();