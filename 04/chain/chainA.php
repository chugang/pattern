<?php
//职责链模式，练习
abstract class Handler{
	protected $_handler;
	
	public function setSuccessor($handler){
		$this->_handler = $handler;
	}
	
	abstract function handleRequest($request); 
}

class ConcreteHandlerZero extends Handler{
	public function handleRequest($request){
		if($request === 0){
			printf('%d<br />',$request);
		}else{
			$this->_handler->handleRequest($request);
		}
	}
}

class ConcreteHandlerEven extends Handler{
	public function handleRequest($request){
		if($request % 2 === 0){
			printf('%d is even<br />',$request);
		}else{
			$this->_handler->handleRequest($request);
		}
	}
}

class ConcreteHandlerOdd extends Handler{
	public function handleRequest($request){
		if($request % 2){
			printf('%d is odd<br />',$request);
		}else{
			$this->_handler->handleRequest($request);
		}
	}
}

//test
class Client{
	public static function main(){
		$objZeroHandler = new ConcreteHandlerZero();
		$objEvenHandler = new ConcreteHandlerEven();
		$objOddHandler = new ConcreteHandlerOdd();
		
		$objZeroHandler->setSuccessor($objEvenHandler);
		$objEvenHandler->setSuccessor($objOddHandler);
		
		foreach(array(5,4,3,0) as $v){
			$objZeroHandler->handleRequest($v);
		}
	}
}

Client::main();