<?php
class Request{
	public $requestType;
	public $number;
	public $content;
	
	public function __construct($requestType,$number,$content){
		$this->requestType = $requestType;
		$this->number = $number;
		$this->content = $content;
	}
}
//加薪职责链模式
abstract class Manager{
	protected $name;
	
	//管理者的上级
	protected $superior;
	
	public function __construct($name){
		$this->name = $name;
	}
	
	//设置管理者的上级
	public function setSuperior($superior){
		$this->superior = $superior;
	}
	
	//申请请求
	abstract public function requestApplications($request);
}

//经理
class CommonManager extends Manager{
	public function __construct($name){
		parent::__construct($name);
	}
	
	public function requestApplications($request){
		if($request->requestType == '请假' && $request->number <= 2){
			$format = '%s：%s 数量 %d 被批准<br />';
			printf($format,$this->name,$request->content,$request->number);
		}else{
			$this->superior->requestApplications($request);
		}
	}
}

//总监
class Majordomo extends Manager{
	public function __construct($name){
		parent::__construct($name);
	}
	
	public function requestApplications($request){
		if($request->requestType == '请假' && $request->number <= 5){
			$format = '%s：%s 数量 %d 被批准<br />';
			printf($format,$this->name,$request->content,$request->number);
		}else{
			$this->superior->requestApplications($request);
		}
	}
}

//总经理
class GeneralManager extends Manager{
	public function __construct($name){
		parent::__construct($name);
	}
	
	public function requestApplications($request){
		if($request->requestType == '请假'){
			$format = '%s：%s 数量 %d 被批准<br />';
			printf($format,$this->name,$request->content,$request->number);
		}elseif($request->requestType == '加薪' && $request->number <= 500){
			$format = '%s：%s 数量 %d 被批准<br />';
			printf($format,$this->name,$request->content,$request->number);
		}elseif($request->requestType == '加薪' && $request->number > 500){
			$format = '%s：%s 数量 %d 再说吧<br />';
			printf($format,$this->name,$request->content,$request->number);
		}
	}
}

//test
class Client{
	public static function main(){
		$jim = new CommonManager('Jim');
		$kate = new Majordomo('Kate');
		$bill = new GeneralManager('Bill');
		$jim->setSuperior($kate);
		$kate->setSuperior($bill);
		
		$requestType = '请假';
		$number = 31;
		$content = '小菜请假';
		$request = new Request($requestType,$number,$content);
		$jim->requestApplications($request);
		
		$requestType = '加薪';
		$number = 31;
		$content = '小菜加薪';
		$request = new Request($requestType,$number,$content);
		$jim->requestApplications($request);
		
	}
}

Header('Content-Type:text/html;charset=utf-8');
Client::main();