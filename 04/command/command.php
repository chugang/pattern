<?php
interface Command{
	public function execute();
}

class ConcreteCommand implements Command{
	private $_receiver;
	public function __construct($receiver){
		$this->_receiver = $receiver;
	}
	
	public function execute(){
		$this->_receiver->action();
	}
}

class Receiver{
	private $_name;
	public function __construct($name)
	{
		$this->_name = $name;
	}
	
	public function action(){
		printf('%s',$this->_name . ' do action.<br />');
	}
}

class Invoker{
	private $_command;
	public function __construct($command){
		$this->_command = $command;
	}
	
	public function action(){
		$this->_command->execute();
	}
}

class Client{
	public static function main(){
		$receiver = new Receiver('jaky');
		$command = new ConcreteCommand($receiver);
		$invoker = new Invoker($command);
		$invoker->action();
	}
}

Client::main();