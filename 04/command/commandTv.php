<?php
interface Command{
	public function execute();
}
//tv command
//命令接收者
class Tv{
	public $currentChannel = 0;
	
	public function turnOn(){
		printf('%s','The televison is on<br />');
	}
	
	public function turnOff(){
		printf('%s','The televison is off<br />');
	}
	
	public function changeChannel($channel){
		$this->currentChannel = $channel;
		printf('%s','Now TV channel is ' . $this->currentChannel . '<br />');
	}
}

//开机命令
class CommandOn implements Command{
	private $myTv;
	
	public function __construct($tv){
		$this->myTv = $tv;
	}
	
	public function execute(){
		$this->myTv->turnOn();
	}
}

//关机命令
class CommandOff implements Command{
	private $myTv;
	
	public function __construct($tv){
		$this->myTv = $tv;
	}
	
	public function execute(){
		$this->myTv->turnOff();
	}
}

//频道切换命令
class CommandChange implements Command{
	private $myTv;
	private $channel;
	
	public function __construct($tv,$channel){
		$this->myTv = $tv;
		$this->channel = $channel;
	}
	
	public function execute(){
		$this->myTv->changeChannel($this->channel);
	}
}

//遥控器
class Control{
	private $onCommand;
	private $offCommand;
	private $changeChannel;
	
	public function __construct($on,$off,$channel){
		$this->onCommand = $on;
		$this->offCommand = $off;
		$this->changeChannel = $channel;
	}
	
	public function turnOn(){
		$this->onCommand->execute();
	}
	
	public function turnOff(){
		$this->offCommand->execute();
	}
	
	public function changeChannel(){
		$this->changeChannel->execute();
	}
}

//test
class Client{
	public static function main(){
		$myTv = new Tv();
		$on = new CommandOn($myTv);
		$off = new CommandOff($myTv);
		$channel = new CommandChange($myTv,2);
		
		$control = new Control($on,$off,$channel);
		$control->turnOn();
		$control->changeChannel();
		$control->turnOff();
		$control->turnOn();
	}
}

Client::main();


