<?php
abstract class Component
{
	protected $name;
	
	public function __construct($name)
	{
		$this->name	=	$name;
	}
	
	abstract public function add($c);
	abstract public function remove($c);
	abstract public function display($depth);
	abstract public function getString($str,$depth);
}