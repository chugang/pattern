<?php
require_once 'concrete.php';

$root = new Composite('root');
$root->add(new Leaf('A'));
$root->add(new Leaf('B'));

$comp = new Composite('X');
$comp->add(new Leaf('XA'));
$comp->add(new Leaf('XB'));
$root->add($comp);

$comp2 = new Composite('XY');
$comp2->add(new Leaf('XYA'));
$comp2->add(new Leaf('XYB'));
$root->add($comp2);

$root->add(new Leaf('C'));

$leafD = new Leaf('D');
$root->add($leafD);
//$root->remove($leafD);

$root->display(1);