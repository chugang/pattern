<?php
require_once 'abstract.php';
class Leaf extends Component
{
	public function add($c)
	{
		printf('%s','Can not add to a leaf<br />');
	}
	
	public function remove($c)
	{
		printf('%s','Can not remove from a leaf<br />');
	}
	
	public function display($depth)
	{
		printf('%s',self::getString('-', $depth) . $this->name . '<br />');
	}
	
	public function getString($str, $depth)
	{
		$string = '';
		for($i=0;$i<$depth;$i++)
		{
			$string .= $str;
		}
		return $string;
	}
}

class Composite extends Component
{
	private $children;
	
	public function add($c)
	{
		$this->children[] = $c;
	}
	
	public function remove($c)
	{
		foreach($this->children as $k=>$child)
		{
			if($child === $c)
			{
				unset($this->children[$k]);
			}
		}
	}
	
	public function display($depth)
	{
		printf('%s',self::getString('-', $depth) . $this->name . '<br />');
		
		foreach($this->children as $child)
		{
			$child->display($depth + 2);
		}
	}
	
	public function getString($str, $depth)
	{
		$string = '';
		for($i=0;$i<$depth;$i++)
		{
		    $string .= $str;
		}
		return $string;
	}
}