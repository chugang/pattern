<?php
//decorator pattern
abstract class Component{
	public abstract function operation();
}

class ConcreteComponent extends Component{
	public function operation(){
		printf('%s<br />','具体对象的操作');
	}
}

abstract class Decorator extends Component{
	protected $component;
	
	public function setComponent($component){
		$this->component = $component;
	}
	
	public function operation(){
		if($this->component != null){
			$this->component->operation();
		}
	}
}

class ConcreteDecoratorA extends Decorator{
	private $addState;
	
	public function operation(){
		parent::operation();
		$this->addState = 'New State';
		printf('%s<br />','具体操作对象A的操作');
	}
}

class ConcreteDecoratorB extends Decorator{
	public function operation(){
		parent::operation();
		$this->addBehavior();
		printf('%s<br />','具体操作对象B的操作');
	}
	
	public function addBehavior(){
		printf('%s<br />','B特有的方法');
	}
}

class ConcreteDecoratorC extends Decorator{
	public function operation(){
		parent::operation();
		$this->addMoney();
		printf('%s<br />','具体操作对象C的操作');
	}
	
	public function addMoney(){
		printf('%s<br />','奖励十万块');
	}
}

class Client{
	public static function main(){
		$c = new ConcreteComponent();
		$d1 = new ConcreteDecoratorA();
		$d2 = new ConcreteDecoratorB();
		$d3 = new ConcreteDecoratorC();
		
		$d1->setComponent($c);
		$d2->setComponent($d1);
		$d3->setComponent($d2);
		$d3->operation();
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();