<?php
//四个子系统的类
class SubSystemOne{
	public function methodOne(){
		printf('	%s<br />','子系统方法一');
	}
}

class SubSystemTwo{
	public function methodTwo(){
		printf('	%s<br />','子系统方法二');
	}
}

class SubSystemThree{
	public function methodThree(){
		printf('	%s<br />','子系统方法三');
	}
}

class SubSystemFour{
	public function methodFour(){
		printf('%s<br />','子系统方法四');
	}
}

//外观类
class Facade{
	private $one;
	private $two;
	private $three;
	private $four;
	
	public function __construct(){
		$this->one = new SubSystemOne();
		$this->two = new SubSystemTwo();
		$this->three = new SubSystemThree();
		$this->four = new SubSystemFour();
	}
	
	public function methodA(){
		printf('%s<br />','方法组A() ---');
		$this->one->methodOne();
		$this->two->methodTwo();
		$this->four->methodFour();
	}
	
	public function methodB(){
		printf('%s<br />','方法组B() ---');
		$this->two->methodTwo();
		$this->three->methodThree();
	}
}

//client
class Client{
	public static function main(){
		$facade = new Facade();
		
		$facade->methodA();
		$facade->methodB();
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();