<?php
class Operation{
	protected $_numberA = 0;
	protected $_numberB = 0;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	public function getResult(){
		$result = 0;
		return $result;
	}
}

class OperationAdd extends Operation{
	public function getResult(){
		$result = 0;
		$result = $this->_numberA + $this->_numberB;
		return $result;
	}
}

class OperationSub extends Operation{
	public function getResult(){
		$result = 0;
		$result = $this->_numberA - $this->_numberB;
		return $result;
	}
}

class OperationMul extends Operation{
	public function getResult(){
		$result = 0;
		$result = $this->_numberA * $this->_numberB;
		return $result;
	}
}

class OperationDiv extends Operation{
	public function getResult(){
		$result = 0;
		try{
			if($this->_numberB === 0){
				throw new Exception('除数不能为0');
			}
		}catch(Exception $e){
			/**
			 * 这里需要注意，直接返回，即return $e->getMessage()，不能阻止程序继续执行。
			 */
			printf('%s<br />',$e->getMessage());
			return false;
		}
		
		$result = $this->_numberA / $this->_numberB;
		return $result;
	}
}

class OperationFactory{
	public static function createOperation($operate){
		$oper = '';
		switch($operate){
			case '+':
				$oper = new OperationAdd();
				break;
			case '-':
				$oper = new OperationSub();
				break;
			case '*':
				$oper = new OperationMul();
				break;
			case '/':
				$oper = new OperationDiv();
				break;
		}
		return $oper;
	}
}

class Client{
	public static function main(){
		
		$oper = OperationFactory::createOperation('+');
		$oper->__set('_numberA',1);
		$oper->__set('_numberB',2);
		$result = $oper->getResult();
		//var_dump($result);exit;
		printf('%d<br />',$result);
		
		$oper = OperationFactory::createOperation('/');
		$oper->__set('_numberA',15);
		$oper->__set('_numberB',10);
		$result = $oper->getResult();
		if($result !==false){
			printf('%s<br />',$result);
		}
		
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();