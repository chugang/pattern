<?php
//工厂方法模式
class Operation{
	protected $_numberA = 0;
	protected $_numberB = 0;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	public function getResult(){
		$result = 0;
		return $result;
	}
}

class OperationAdd extends Operation{
	public function getResult(){
		$result = 0;
		$result = $this->_numberA + $this->_numberB;
		return $result;
	}
}

class OperationSub extends Operation{
	public function getResult(){
		$result = 0;
		$result = $this->_numberA - $this->_numberB;
		return $result;
	}
}

class OperationMul extends Operation{
	public function getResult(){
		$result = 0;
		$result = $this->_numberA * $this->_numberB;
		return $result;
	}
}

class OperationDiv extends Operation{
	public function getResult(){
		$result = 0;
		try{
			if($this->_numberB === 0){
				throw new Exception('除数不能为0');
			}
		}catch(Exception $e){
			/**
			 * 这里需要注意，直接返回，即return $e->getMessage()，不能阻止程序继续执行。
			 */
			printf('%s<br />',$e->getMessage());
			return false;
		}
		
		$result = $this->_numberA / $this->_numberB;
		return $result;
	}
}

//工厂接口
interface IFactory{
	public function createOperation();
}

//加法工厂
class AddFactory implements IFactory{
	public function createOperation(){
		return new operationAdd();
	}
}

//减法工厂
class SubFactory implements IFactory{
	public function createOperation(){
		return new operationSub();
	}
}

//乘法工厂
class MulFactory implements IFactory{
	public function createOperation(){
		return new operationMul();
	}
}

//除法工厂
class DivFactory implements IFactory{
	public function createOperation(){
		return new operationDiv();
	}
}

