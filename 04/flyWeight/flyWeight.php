<?php
class User{
	private $name;
	
	public function __construct($name){
		$this->name = $name;
	}
	
	public function getName(){
		return $this->name;
	}
}

//网站抽象类
abstract class WebSite{
	abstract public function using($user);
}

//具体网站类
class ConcreteWebSite extends WebSite{
	private $name;
	
	public function __construct($name){
		$this->name = $name;
	}
	
	public function using($user){
		printf('%s','网站分类：' . $this->name . ' 用户： ' . $user->getName() . '<br />');
	}
}

//网站工厂类
class WebSiteFactory{
	private $flyWeights = array();
	
	//获得网站分类
	public function getWebSiteCategory($key){
		if(!array_key_exists($key,$this->flyWeights)){
			$this->flyWeights[$key] = new ConcreteWebSite($key);
		}
		return $this->flyWeights[$key];
	}
	
	//获得网站分类总数
	public function getWebSiteCount(){
		return count($this->flyWeights);
	}
}

//test
class Client{
	public static function main(){
		$f = new WebSiteFactory();
		$fx = $f->getWebSiteCategory('产品展示');
		$fx->using(new User('小菜'));
		
		$fy = $f->getWebSiteCategory('产品展示');
		$fy->using(new User('大鸟'));
		
		$fz = $f->getWebSiteCategory('产品展示');
		$fz->using(new User('孙悟空'));
		
		$fa = $f->getWebSiteCategory('博客');
		$fa->using(new User('老顽童'));
		
		$fb = $f->getWebSiteCategory('博客');
		$fb->using(new User('张飞'));
	}
}

Header('Content-Type:text/html;charset=utf-8');
Client::main();
