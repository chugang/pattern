<?php
abstract class AbstractExpression{
	//private $lists;
	public abstract function interpret($context);
	
	/*
	public function attach($exp){
		$this->lists[] = $exp;
	}
	*/
}

class TerminalExpression extends AbstractExpression{
	public function interpret($context){
		printf('%s<br />','终端解释器');
	}
}

class NonterminalExpression extends AbstractExpression{
	public function interpret($context){
		printf('%s<br />','非终端解释器');
	}
}

class Context{
	private $input;
	private $output;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
}

class Client{
	public static function main(){
		$context = new Context();
		$lists = array();
		$lists[] = new TerminalExpression();
		$lists[] = new NonterminalExpression();
		$lists[] = new TerminalExpression();
		$lists[] = new TerminalExpression();
		
		foreach($lists as $exp){
			$exp->interpret($context);
		}
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();