<?php
//interpreter music
//演奏内容
class PlayContext{
	//演奏文本
	private $text;
	public function setText($value){
		$this->text = $value;
	}
	
	public function getText(){
		return $this->text;
	}
}

//表达式类
abstract class Expression{
	//解释器
	public function interpret($context){
		$text = $context->getText();
		if(strlen($text) == 0){
			return false;
		}else{
			$playKey = substr($text,0,1);
			$text = substr($text,2);
			$emptyPos = strpos($text,' ');
			$playValue = floatval(substr($text,0,$emptyPos));
			$text = substr($text,$emptyPos+1);
			$context->setText($text);
			
			$this->execute($playKey,$playValue);
		}
	}
	
	//执行
	public abstract function execute($key,$value);
}

//音符类
class Note extends Expression{
	public function execute($key,$value){
		$note = '';
		switch($key){
			case 'C':
				$note = '1';
				break;
			case 'D':
				$note = '2';
				break;
			case 'E':
				$note = '3';
				break;
			case 'F':
				$note = '4';
				break;
			case 'G':
				$note = '5';
				break;
			case 'A':
				$note = '6';
				break;
			case 'B':
				$note = '7';
				break;
		}
		printf('%s ',$note);
	}
}

//音阶类
class Scale extends Expression{
	public function execute($key,$value){
		$scale = '';
		switch($value){
			case 1:
				$scale = '低音';
				break;
			case 2:
				$scale = '中音';
				break;
			case 3:
				$scale = '高音';
				break;
		}
		printf('%s ',$scale);
	}
}

class Speed extends Expression{
	public function execute($key,$value){
		$speed = '';
		if($value < 500){
			$speed = '快速';
		}elseif($value > 1000){
			$speed = '慢速';
		}else{
			$speed = '中速';
		}
		printf('%s ',$speed);
	}
}

class Client{
	public static function main(){
		$context = new PlayContext();
		//音乐-上海滩
		printf('%s:','上海滩');
		$context->setText('T 500 O 2 E 0.5 G 0.5 A 3 E 0.5 G 0.5 D 3 E 0.5 G 0.5 A 0.5 O 3 C 1 0 2 A 0.5 G 1 C 0.5 E 0.5 D 3 ');
		$expression = '';
		while(strlen($text = $context->getText()) > 0){
			$str = substr($text,0,1);
			switch($str){
				case 'O':
					$expression = new Scale();
					break;
				case 'T':
					$expression = new Speed();
					break;
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'A':
				case 'B':
					$expression = new Note();
					break;
			}
			$expression->interpret($context);
		}
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
set_time_limit(20);
Client::main();