<?php
abstract class Iteratora{
	public abstract function first();
	public abstract function next();
	public abstract function isDone();
	public abstract function currentItem();
}

abstract class Aggregate{
	public abstract function createIterator();
}

class ConcreteIterator extends Iteratora{
	private $aggregate;
	private $current = 0;
	
	public function __construct($aggregate){
		$this->aggregate = $aggregate->getItems();
	}
	
	public function first(){
		return $this->aggregate[0];
	}
	
	public function next(){
		$ret = false;
		$this->current++;
		if($this->current < count($this->aggregate)){
			$res = $this->aggregate[$this->current];
		}
		return $ret;
	}
	
	public function isDone(){
		return ($this->current >= count($this->aggregate)?true:false);
	}
	
	public function currentItem(){
		return $this->aggregate[$this->current];
	}
}

class ConcreteAggregate extends Aggregate{
	private $itmes = array();
	
	public function createIterator(){
		return new ConcreteIterator($this);
	}
	
	public function getCount(){
		return count($this->items);
	}
	
	public function attachItems($item){
		$this->items[] = $item;
	}
	
	public function getItems(){
		return $this->items;
	}
}

class Client{
	public static function main(){
		$a = new ConcreteAggregate();
		$a->attachItems('大鸟');
		$a->attachItems('小菜');
		$a->attachItems('老外');
		$a->attachItems('行李');
		$a->attachItems('公交公司员工');
		
		$i = new ConcreteIterator($a);
		$items = $i->first();
		while(!$i->isDone()){
			printf('%s请买车票<br />',$i->currentItem());
			$i->next();
		}
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();