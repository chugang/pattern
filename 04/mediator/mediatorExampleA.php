<?php
//中介者模式

//抽象中介者类
abstract class Mediator{
	public abstract function send($message,$colleague);
}

//抽象同事类
abstract class Colleague{
	protected $mediator;
	
	public function __construct($mediator){
		$this->mediator = $mediator;
	}
}

//具体中介者类
class ConcreteMediator extends Mediator{
	private $colleague1;
	private $colleague2;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	public function send($message,$colleague){
		if($colleague == $this->colleague1){
			$this->colleague2->notify($message);
		}else{
			$this->colleague1->notify($message);
		}
	}
}

//具体同事类
class ConcreteColleague1 extends Colleague{
	public function __construct($mediator){
		parent::__construct($mediator);
	}
	
	public function send($message){
		$this->mediator->send($message,$this);
	}
	
	public function notify($message){
		$format = '同事1得到的信息：%s<br />';
		printf($format,$message);
	}
}

//具体同事类
class ConcreteColleague2 extends Colleague{
	public function __construct($mediator){
		parent::__construct($mediator);
	}
	
	public function send($message){
		$this->mediator->send($message,$this);
	}
	
	public function notify($message){
		$format = '同事2得到的信息：%s<br />';
		printf($format,$message);
	}
}

//client
class Client{
	public static function main(){
		$m = new ConcreteMediator();
		
		$c1 = new ConcreteColleague1($m);
		$c2 = new ConcreteColleague2($m);
		
		$m->__set('colleague1',$c1);
		$m->__set('colleague2',$c2);
		
		$c1->send('吃饭了吗？');
		$c2->send('没有。你打算请客吗？');
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();
