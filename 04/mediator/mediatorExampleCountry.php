<?php
//mediator pattern,country
//联合国
abstract class UnitedNations{
	public abstract function send($message,$colleague);
}

//抽象国家类
abstract class Country{
	protected $mediator;
	
	public function __construct($mediator){
		$this->mediator = $mediator;
	}
}

//美国
class USA extends Country{
	public function __construct($mediator){
		parent::__construct($mediator);
	}
	
	public function send($message){
		$this->mediator->send($message,$this);
	}
	
	public function getMessage($message){
		$format = '美国获得对方信息：%s<br />';
		printf($format,$message);
	}
}

//伊拉克
class Iraq extends Country{
	public function __construct($mediator){
		parent::__construct($mediator);
	}
	
	public function send($message){
		$this->mediator->send($message,$mediator);
	}
	
	public function getMessage($message){
		$format = '伊拉克获得对方信息：%s<br />';
		printf($format,$message);
	}
}

//联合国安理会
class UnitedNationsSecurityCouncil extends UnitedNations{
	private $colleague1;
	private $colleague2;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	public function send($message,$colleague){
		if($colleague == $this->colleague1){
			$this->colleague2->getMessage($message);
		}else{
			$this->colleague1->getMessage($message);
		}
	}
}

//client
class Client{
	public static function main(){
		$m = new UnitedNationsSecurityCouncil();
		$c1 = new USA($m);
		$c2 = new Iraq($m);
		
		$m->__set('colleague1',$c1);
		$m->__set('colleague2',$c2);
		
		$c1->send('不准研制核武器，否则要发动战争！');
		$c1->send('我们没有核武器，也不怕侵略！');
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();