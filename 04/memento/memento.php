<?php
//发起人角色
class Originator{
	private $_state;
	
	public function __construct(){
		$this->_state = '';
	}
	
	//创建备忘录
	public function createMemento(){
		return new Memento($this->_state);
	}
	
	//将发起人恢复到备忘录对象记录的状态上
	public function restoreMemento(Memento $memento){
		$this->_state = $memento->getState();
	}
	
	public function setState($state){
		$this->_state = $state;
	}
	
	public function getState(){
		return $this->_state;
	}
	
	public function showState(){
		printf('%s',$this->_state . '<br />');
	}
}

//备忘录角色
class Memento{
	private $_state;
	
	public function __construct($state){
		$this->setState($state);
	}
	
	public function getState(){
		return $this->_state;
	}
	
	public function setState($state){
		$this->_state = $state;
	}
}

//负责人角色
class CareTaker{
	private $_memento;
	
	public function getMemento(){
		return $this->_memento;
	}
	
	public function setMemento(Memento $memento){
		$this->_memento = $memento;
	}
}

//test
class Client{
	public static function main(){
		
		//创建目标对象
		$org = new Originator();
		$org->setState('open');
		$org->showState();
		
		//创建备忘录
		$memento = $org->createMemento();
		
		//通过CareTaker保存此备忘录
		$careTaker = new CareTaker();
		$careTaker->setMemento($memento);
		
		//改变目标对象的状态
		$org->setState('close');
		$org->showState();
		
		//还原操作
		$org->restoreMemento($careTaker->getMemento());
		$org->showState();
	}
}

Client::main();