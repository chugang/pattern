<?php
class Person{
	private $name;
	private $sex;
	private $age;
	
	/*
	public function __construct($name,$sex,$age){
		$this->name = $name;
		$this->sex = $sex;
		$this->age = $age;
	}
	*/
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	public function createMemento(){
		return new Memento($this->name,$this->sex,$this->age);
	}
	
	public function restoreMemento(Memento $m){
		$this->name = $m->__get('name');
		$this->sex = $m->__get('sex');
		$this->age = $m->__get('age');
	}
	
	public function introduce(){
		printf('My name is %s.I am a %s,%d years old<br />',$this->name,$this->sex,$this->age);
	}
}

class Memento{
	private $name;
	private $sex;
	private $age;
	
	public function __construct($name,$sex,$age){
		$this->name = $name;
		$this->sex = $sex;
		$this->age = $age;
	}
	
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
}

class Client{
	public static function main(){
		$p = new Person();
		$p->__set('name','Kate');
		$p->__set('sex','girl');
		$p->__set('age',12);
		$kate = $p->createMemento();
		$p->introduce();
		
		$p->__set('sex','woman');
		$p->__set('age',34);
		$p->introduce();
		
		$p->restoreMemento($kate);
		$p->introduce();
	}
}

Client::main();

