<?php
//简化的memento
class Memento{
	private $name;
	private $sex;
	private $age;
	
	public function __construct($name,$sex,$age){
		$this->name = $name;
		$this->sex = $sex;
		$this->age = $age;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	public function __set($property,$value){
		$this->$property = $value;
	}
}

class Person{
	private $name;
	private $sex;
	private $age;
	
	public function createMemento(){
		return new Memento($this->name,$this->sex,$this->age);
	}
	
	public function restoreMemento(Memento $memento){
		$this->name = $memento->__get('name');
		$this->sex = $memento->__get('sex');
		$this->age = $memento->__get('age');
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function introduce(){
		printf('My name is %s.I\'m a %s,%d years old.<br />',$this->name,$this->sex,$this->age);
	}
	
}

//test
class Client{
	public static function main(){
		$p = new Person();
		$p->__set('name','Jim');
		$p->__set('sex','boy');
		$p->__set('age',32);
		$jim = $p->createMemento();
		$p->introduce();
		
		$p->__set('name','Jim');
		$p->__set('sex','man');
		$p->__set('age',64);
		$p->introduce();
		
		$p->restoreMemento($jim);
		$p->introduce();
	}
}

Client::main();