<?php
//简化版的备忘录模式
class Memento{
	private $sex;
	private $age;
	
	public function __construct(Person $person){
		$this->sex = $person->sex;
		$this->age = $person->age;
	}
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	
}

class Person{
	private $sex;
	private $age;
	
	public function createMemento(){
		return new Memento($this);
	}
	
	public function restoreMemento(Memento $m){
		$this->sex = $m->__get('sex');
		$this->age = $m->__get('age');
	}
	
	public function introduce(){
		printf('I am a %s,%d years old.<br />',$this->sex,$this->age);
	}
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
}

class Client{
	public static function main(){
		$p = new Person();
		$p->__set('sex','man');
		$p->__set('age',21);
		$personA = $p->createMemento();
		$p->introduce();
		
		$p->__set('sex','woman');
		$p->__set('age',42);
		$p->introduce();
		
		$p->restoreMemento($personA);
		$p->introduce();
		
	}
}

//test
Client::main();
