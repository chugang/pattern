<?php
//抽象原型类
abstract class Prototype{
	private $id;
	
	public function __construct($id){
		$this->id = $id;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public abstract function copy();
}

class ConcretePrototypeA extends Prototype{
	public function copy(){
		return clone($this);
	}
}

class Client{
	public static function main(){
		$p1 = new ConcretePrototypeA('I');
		$c1 = $p1->copy();
		var_dump($p1,$c1);
	}
}

//test
Client::main();