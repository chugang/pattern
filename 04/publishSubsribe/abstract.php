<?php
/**
 * 观察者模式，又叫发布订阅者模式
 * @author ganghong
 *
 */
abstract class Subject
{
	private $observers;			//观察者
	
	//增加观察者
	public function addObserver($observer)
	{
		$this->observers[]	=	$observer;
	}
	
	//移除观察者
	public function detachObserver($observer)
	{
		foreach($this->observers as $k=>$v)
		{
			if($v	===	$observer)
			{
				unset($this->observers[$k]);
			}
		}
	}
	
	//通知
	public function notify()
	{
		foreach($this->observers as $observer)
		{
			$observer->update();
		}
	}
}


abstract class Observer
{
	protected  $name;
	protected  $observerState;
	protected  $subject;
	
	abstract public function update();
	
	public function getSubject()
	{
		return $this->subject;
	}
	
	public function setSubject($subject)
	{
		$this->subject	=	$subject;
	}
}