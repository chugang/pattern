<?php
require_once('concrete.php');
$s	=	new ConcreteSubject();
$ox	=	new ConcreteObserver($s, 'X');
$oy	=	new ConcreteObserver($s, 'Y');
$oz	=	new ConcreteObserver($s, 'Z');
$s->addObserver($ox);
$s->addObserver($oy);
$s->addObserver($oz);
$s->setSubjectState('Boss is coming!');
$s->notify();
$s->detachObserver($oy);
$s->notify();
$os	=	new StockObserver($s,'Jim');
$s->addObserver($os);
$s->notify();
$s->setSubjectState('Boss has left.');
$s->notify();
