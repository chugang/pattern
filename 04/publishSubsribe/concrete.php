<?php
require_once('abstract.php');
class ConcreteSubject extends Subject
{
	private $subjetState;				//具体被观察者状态
	
	public function getSubjectState()
	{
		return $this->subjetState;
	}
	
	public function setSubjectState($state)
	{
		$this->subjetState	=	$state;
	}
}

class ConcreteObserver	extends	Observer
{
	public function __construct($subject,$name)
	{
		$this->subject	=	$subject;
		$this->name	=	$name;
	}
	
	public function update()
	{
		$this->observerState	=	$this->subject->getSubjectState();
		printf("观察者%s,%s<br />",$this->name,$this->observerState);
	}
	
}

class StockObserver extends Observer
{
	public function __construct($subject,$name)
	{
		$this->subject	=	$subject;
		$this->name	=	$name;
	}
	
	public function update()
	{
		$this->observerState	=	$this->subject->getSubjectState();
		printf("观察者%s,%s，关闭股票行情，继续工作！<br />",$this->name,$this->observerState);
	}
}
