<?php
//singleton
class Singleton{
	private static $singleton;
	
	private function __construct(){
		
	}
	
	public static function getInstance(){
		if(is_null(self::$singleton)){
			self::$singleton = new Singleton();
		}
		return self::$singleton;
	}
	
	
}

class Client{
	public static function main(){
		//$s = new Singleton();
		$s = Singleton::getInstance();
		var_dump($s);
		
		$s1 = Singleton::getInstance();
		var_dump($s1);
		
		if($s === $s1){
			printf('%s','They are same<br />');
		}
	}
}

Client::main();
