<?php
//singleton
class Singleton{
	private static $instance;
	
	private function __construct(){
		
	}
	
	public static function getInstance(){
		if(is_null(self::$instance)){
			self::$instance = new Singleton();
		}
		return self::$instance;
	}
}

class Client{
	public static function main(){
		$s1 = Singleton::getInstance();
		var_dump($s1);
		$s2 = Singleton::getInstance();
		var_dump($s2);
		
		if($s1 === $s2){
			printf('%s','They are same.<br />');
		}
	}
}

Client::main();