<?php

    abstract class baseAgent { //抽象策略类
        abstract function PrintPage();
    }
    //用于客户端是IE时调用的类（环境角色）
    class ieAgent extends baseAgent {
        function PrintPage() {
            return 'IE';
        }
    }
    //用于客户端不是IE时调用的类（环境角色）
    class otherAgent extends baseAgent {
        function PrintPage() {
            return 'not IE';
        }
    }
	
	class fireAgent extends baseAgent{
		function PrintPage() {
			return 'FireFox';
		}
	}
    class Browser {
		private $object;
		//具体策略角色
        public function call() {
            return $this->object->PrintPage ();
        }
		
		public function setObject($object)
		{
			$this->object = $object;
		}
    }
    $bro = new Browser ();
	$bro->setObject(new ieAgent ());
    echo $bro->call () . '<br />';
	
	$bro->setObject(new otherAgent ());
	echo $bro->call () . '<br />';
    
	$bro->setObject(new fireAgent());
	echo $bro->call() . '<br />';
?>