<?php
interface FlyBehavior{
	public function fly();
}

class FlyWithWings implements FlyBehavior{
	public function fly(){
		return 'fly with wings';
	}
}

class FlyWithNoWing implements FlyBehavior{
	public function fly(){
		return 'fly with no wing';
	}
}

class FlyWithRocket implements FlyBehavior{
	public function fly(){
		return 'fly with rocket';
	}
}

class Duck{
	private $flyBehavior;
	
	public function setFlyBehavior($flyBehavior){
		$this->flyBehavior = $flyBehavior;
	}
	
	public function act(){
		$fly = $this->flyBehavior->fly();
		printf('%s',$fly . '<br />');
	}
	
}

class RobotDuck extends Duck{
}

$rduck = new RobotDuck();

$rduck->setFlyBehavior(new FlyWithNoWing());
$rduck->act();

$rduck->setFlyBehavior(new FlyWithWings());
$rduck->act();

$rduck->setFlyBehavior(new FlyWithRocket());
$rduck->act();