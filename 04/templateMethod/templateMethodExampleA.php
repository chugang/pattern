<?php
abstract class AbstractClass{
	public abstract function primitiveOperation1();
	public abstract function primitiveOperation2();
	
	public function templateMethod(){
		$this->primitiveOperation1();
		$this->primitiveOperation2();
	}
}

class ConcreteClassA extends AbstractClass{
	public function primitiveOperation1(){
		printf('%s<br />','具体类A方法1实现');
	}
	
	public function primitiveOperation2(){
		printf('%s<br />','具体类A方法2实现');
	}
}

class ConcreteClassB extends AbstractClass{
	public function primitiveOperation1(){
		printf('%s<br />','具体类B方法1实现');
	}
	
	public function primitiveOperation2(){
		printf('%s<br />','具体类B方法2实现');
	}
}

class Client{
	public static function main(){
		$c = new ConcreteClassA();
		$c->templateMethod();
		
		$c = new ConcreteClassB();
		$c->templateMethod();
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();