<?php
//vistor pattern person
abstract class Action{
	
	protected $name;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	
	//得到男人结论或反应
	public abstract function getManConclusion($concreteElemetA);
	//得到女人结论或反应
	public abstract function getWomanConclusion($concreteElementB);
}
abstract class Person{
	protected $action;
	protected $name;
	
	public function __set($property,$value){
		$this->$property = $value;
	}
	
	public function __get($property){
		return $this->$property;
	}
	
	//接受
	public abstract function accept($vistor);
}

//男人
class Man extends Person{
	
	public function __construct(){
		$this->name = '男人';
	}
	public function accept($vistor){
		$vistor->getManConclusion($this);
	}
	
	
}

//女人
class Woman extends Person{
	
	public function __construct(){
		$this->name = '女人';
	}
	public function accept($vistor){
		$vistor->getWomanConclusion($this);
	}
}

//成功
class Success extends Action{
	public function __construct(){
		$this->name = '成功';
	}
	public function getManConclusion($concreteElementA){
		$format = '%s%s时，背后多半有一个伟大的女人。<br >';
		printf($format,$concreteElementA->name,$this->name);
	}
	
	public function getWomanConclusion($concreteElementB){
		$format = '%s%s时，背后大多有一个不成功的男人。<br >';
		printf($format,$concreteElementB->name,$this->name);
	}
}

//结婚
class Marriage extends Action{
	public function __construct(){
		$this->name = '结婚';
	}
	
	public function getManConclusion($concreteElementA){
		$format = '%s%s时，感叹道：恋爱游戏结束时，“有妻徒刑遥遥无期”。<br >';
		printf($format,$concreteElementA->name,$this->name);
	}
	
	public function getWomanConclusion($concreteElementB){
		$format = '%s%s时，欣慰曰：爱情长跑路漫漫，婚姻保险保平安。<br >';
		printf($format,$concreteElementB->name,$this->name);
	}
}

//对象结构
class ObjectStructure{
	private $elements = array();
	
	//增加
	public function attach(Person $elemet){
		$this->elements[] = $elemet;
	}
	
	//移除
	public function detach(Person $elemet){
		if(($key = array_search($element,$this->elements)) !== false){
			unset($this->elements[$key]);
		}
	}
	
	//显示
	public function show($vistor){
		foreach($this->elements as $element){
			$element->accept($vistor);
		}
	}
}

//test
class Client{
	public static function main(){
		$o = new ObjectStructure();
		$man = new Man();
		$woman = new Woman();
		$o->attach($man);
		$o->attach($woman);
		
		//成功时的反应
		$o->show(new Success());
		
		//结婚时反应
		$o->show(new Marriage());
		
	}
}
Header('Content-Type:text/html;charset=utf-8');
Client::main();